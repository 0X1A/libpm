// Copyright (C) 2016, Alberto Corona <ac@albertocorona.com>
// All rights reserved. This file is part of core-utils, distributed under the
// BSD 3-clause license. For full terms please see the LICENSE file.

extern crate toml;
extern crate time;
extern crate walkdir;
extern crate rustc_serialize;
extern crate git2;
extern crate curl;

use std::error::*;
use std::fmt;
use std::io;
use std::path::StripPrefixError;

#[derive(Debug)]
pub enum PkgError {
    Io(io::Error),
    PrefixError(StripPrefixError),
    Curl(curl::Error),
    Time(time::ParseError),
    WalkDir(walkdir::Error),
    TomlParse(toml::ParserError),
    TomlDecode(toml::DecodeError),
    JsonEncode(rustc_serialize::json::EncoderError),
    Git(git2::Error),
    NonToml(String),
    NoDesc(String),
    HashMismatch(String, String),
    NoHash(String),
}

impl fmt::Display for PkgError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            PkgError::Io(ref err) => write!(f, "i/o error, {}", err),
            PkgError::PrefixError(ref err) => write!(f, "error trying to strip prefix for {}", err),
            PkgError::Curl(ref err) => write!(f, "curl error {}", err),
            PkgError::Time(ref err) => write!(f, "time error, {}", err),
            PkgError::WalkDir(ref err) => write!(f, "directory walking error, {}", err),
            PkgError::TomlParse(ref err) => write!(f, "toml parsing error, {}", err),
            PkgError::TomlDecode(ref err) => write!(f, "toml decoding error, {}", err),
            PkgError::JsonEncode(ref err) => write!(f, "json encoding error {}", err),
            PkgError::Git(ref err) => write!(f, "git error {}", err),
            PkgError::NonToml(ref file) => write!(f, "'{}' is not a toml file", file),
            PkgError::NoDesc(ref name) => write!(f, "no '{}' section found in PKG.toml", name),
            PkgError::HashMismatch(ref file, ref hash) => {
                write!(f, "hash mismatch: '{}' : '{}'", file, hash)
            }
            PkgError::NoHash(ref file) => write!(f, "no hash for file '{}'", file),
        }
    }
}

impl Error for PkgError {
    fn description(&self) -> &str {
        match *self {
            PkgError::Io(ref err) => err.description(),
            PkgError::PrefixError(ref err) => err.description(),
            PkgError::Curl(ref err) => err.description(),
            PkgError::Time(ref err) => err.description(),
            PkgError::WalkDir(ref err) => err.description(),
            PkgError::TomlParse(ref err) => err.description(),
            PkgError::TomlDecode(ref err) => err.description(),
            PkgError::JsonEncode(ref err) => err.description(),
            PkgError::Git(ref err) => err.description(),
            PkgError::NonToml(..) => "toml file error",
            PkgError::NoDesc(..) => "no 'package' nor 'clean' section found in PKG.toml",
            PkgError::HashMismatch(..) => "hash mismatch",
            PkgError::NoHash(..) => "no hash for file",
        }
    }

    fn cause(&self) -> Option<&Error> {
        match *self {
            PkgError::Io(ref err) => Some(err),
            PkgError::PrefixError(ref err) => Some(err),
            PkgError::Curl(ref err) => Some(err),
            PkgError::Time(ref err) => Some(err),
            PkgError::WalkDir(ref err) => Some(err),
            PkgError::TomlParse(ref err) => Some(err),
            PkgError::TomlDecode(ref err) => Some(err),
            PkgError::JsonEncode(ref err) => Some(err),
            PkgError::Git(ref err) => Some(err),
            PkgError::NonToml(..) => None,
            PkgError::NoDesc(..) => None,
            PkgError::HashMismatch(..) => None,
            PkgError::NoHash(..) => None,
        }
    }
}

impl From<io::Error> for PkgError {
    fn from(err: io::Error) -> PkgError {
        PkgError::Io(err)
    }
}

impl From<StripPrefixError> for PkgError {
    fn from(err: StripPrefixError) -> PkgError {
        PkgError::PrefixError(err)
    }
}

impl From<curl::Error> for PkgError {
    fn from(err: curl::Error) -> PkgError {
        PkgError::Curl(err)
    }
}

impl From<time::ParseError> for PkgError {
    fn from(err: time::ParseError) -> PkgError {
        PkgError::Time(err)
    }
}

impl From<walkdir::Error> for PkgError {
    fn from(err: walkdir::Error) -> PkgError {
        PkgError::WalkDir(err)
    }
}

impl From<toml::ParserError> for PkgError {
    fn from(err: toml::ParserError) -> PkgError {
        PkgError::TomlParse(err)
    }
}

impl From<toml::DecodeError> for PkgError {
    fn from(err: toml::DecodeError) -> PkgError {
        PkgError::TomlDecode(err)
    }
}

impl From<rustc_serialize::json::EncoderError> for PkgError {
    fn from(err: rustc_serialize::json::EncoderError) -> PkgError {
        PkgError::JsonEncode(err)
    }
}

impl From<git2::Error> for PkgError {
    fn from(err: git2::Error) -> PkgError {
        PkgError::Git(err)
    }
}

impl From<PkgError> for Vec<PkgError> {
    fn from(err: PkgError) -> Vec<PkgError> {
        vec![err]
    }
}

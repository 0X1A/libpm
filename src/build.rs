// Copyright (C) 2016, Alberto Corona <ac@albertocorona.com>
// All rights reserved. This file is part of core-utils, distributed under the
// BSD 3-clause license. For full terms please see the LICENSE file.

extern crate toml;
extern crate rustc_serialize;
extern crate tar;
extern crate time;
extern crate walkdir;
extern crate crypto;
extern crate flate2;
extern crate curl;
extern crate ansi_term;

use ansi_term::Colour;
use crypto::digest::Digest;
use crypto::sha2;
use desc::pkg::*;
use error::PkgError;
use ext::{assert_toml, clean_path, parse_toml_file};

use rustc_serialize::{Decodable, Encodable};
use rustc_serialize::json;

use std::env;
use std::fs;
use std::fs::File;
use std::io::prelude::*;

#[cfg(target_family = "unix")]
use std::os::unix::fs::PermissionsExt;
use std::path::{Path, PathBuf};
use std::process::Command;
use walkdir::WalkDir;

/// A high level representation of a tarball file
pub struct TarFile {
    /// Path to file
    pub path: PathBuf,
    /// File descriptor
    pub file: File,
}

impl TarFile {
    /// Creates a new file from `path`
    pub fn new(path: PathBuf) -> Result<TarFile, PkgError> {
        Ok(TarFile {
            file: File::create(&path)?,
            path: PathBuf::from(path),
        })
    }
}


/// Describes target architectures, defaults to the host's architecture using
/// `Arch::default()`
#[allow(non_camel_case_types)]
#[derive(PartialEq,PartialOrd,Debug,RustcEncodable,RustcDecodable,Clone,Eq,Hash)]
pub enum Arch {
    x86_64,
    i686,
    arm,
    aarch64,
    powerpc,
    any,
}

impl Default for Arch {
    fn default() -> Arch {
        match env::consts::ARCH {
            "x86_64" => Arch::x86_64,
            "i686" => Arch::i686,
            "arm" => Arch::arm,
            "aarch64" => Arch::aarch64,
            "powerpc" => Arch::powerpc,
            _ => Arch::any,
        }
    }
}

#[derive(Debug,Default,PartialEq,Clone)]
pub struct PackageBuild {
    package: Option<PackageDesc>,
    clean: Option<CleanDesc>,
}

impl PackageBuild {
    pub fn new() -> PackageBuild {
        Default::default()
    }

    pub fn from_file(file: &str) -> Result<PackageBuild, Vec<PkgError>> {
        assert_toml(file)?;
        let mut pkg_build: PackageBuild = Default::default();
        parse_toml_file(file)
            .and_then(|toml| {
                for (key, table) in toml {
                    match key.as_ref() {
                        "package" => pkg_build.package = PackageDesc::from_toml_table(table).ok(),
                        "clean" => pkg_build.clean = CleanDesc::from_toml_table(table).ok(),
                        _ => (),
                    };
                }
                Ok(pkg_build)
            })
            .map_err(|err| err)
    }

    pub fn print_json(self) {
        println!("{}", Colour::White.bold().paint("[package]"));
        self.package.unwrap_or(PackageDesc::new()).print_json();
        println!("{}", Colour::White.bold().paint("[clean]"));
        self.clean.unwrap_or(CleanDesc::new()).print_json();
    }

    pub fn package(self) -> Option<PackageDesc> {
        self.package
    }

    pub fn clean(self) -> Option<CleanDesc> {
        self.clean
    }
}

/// Description of what to do to clean the build environment
#[derive(RustcDecodable,RustcEncodable,Debug,Default,PartialEq,Clone)]
pub struct CleanDesc {
    script: Option<Vec<String>>,
}

impl CleanDesc {
    pub fn new() -> CleanDesc {
        Default::default()
    }

    pub fn clean(&self) -> Result<(), PkgError> {
        println!("{}",
                 Colour::White.bold().paint("Cleaning build environment"));
        if let Some(script) = self.script.as_ref() {
            self.exec(script)?;
        };
        Ok(println!("{}", Colour::White.bold().paint("Clean succeeded")))
    }
}

#[derive(RustcDecodable,RustcEncodable,Debug,Default,PartialEq)]
pub struct MTree {
    entries: Vec<MTreeEntry>,
}

impl MTree {
    fn new() -> MTree {
        Default::default()
    }

    pub fn write(&self, path: &str) -> Result<(), PkgError> {
        print!("{}", Colour::White.bold().paint("Generating MTREE..."));
        File::create(path)?.write_all(&format!("{}", toml::encode(&self)).as_bytes())?;
        Ok(print!("{}\n", Colour::Green.paint("OK")))
    }

    fn add(&mut self, entry: MTreeEntry) {
        self.entries.push(entry);
    }

    #[cfg(target_family = "unix")]
    pub fn from_dir(path: &str) -> Result<MTree, PkgError> {
        let mut mtree = MTree::new();
        for entry in WalkDir::new(path) {
            let entry = entry?;
            if entry.path() != Path::new("pkg") {
                let mut mtree_entry = MTreeEntry::new();
                let file_name = clean_path(entry.path().to_path_buf());
                mtree_entry.set_path(file_name);
                if !entry.path().is_dir() {
                    mtree_entry.set_checksum(entry.path())?;
                    mtree_entry.set_size(entry.path())?;
                }
                mtree_entry.set_time()?;
                mtree_entry.set_mode(entry.path())?;
                mtree.add(mtree_entry);
            }
        }
        Ok(mtree)
    }
}

#[derive(RustcDecodable,RustcEncodable,Debug,Default,PartialEq)]
pub struct MTreeEntry {
    path: String,
    time: String,
    size: u64,
    checksum: String,
    mode: u32,
}

impl MTreeEntry {
    fn new() -> MTreeEntry {
        Default::default()
    }

    fn set_path(&mut self, path: PathBuf) {
        self.path = self.prepend_path(&path).to_string_lossy().to_string();
    }

    fn set_time(&mut self) -> Result<(), PkgError> {
        match time::strftime("%m%d%Y%H%M%S", &time::now_utc()) {
            Ok(s) => self.time = s,
            Err(e) => return Err(PkgError::Time(e)),
        };
        Ok(())
    }

    fn set_mode(&mut self, path: &Path) -> Result<(), PkgError> {
        let metadata = fs::metadata(path)?;
        self.mode = metadata.permissions().mode();
        Ok(())
    }

    fn set_size(&mut self, path: &Path) -> Result<(), PkgError> {
        match fs::metadata(path) {
            Ok(s) => self.size = s.len(),
            Err(e) => return Err(PkgError::Io(e)),
        };
        Ok(())
    }

    fn set_checksum(&mut self, path: &Path) -> Result<(), PkgError> {
        let mut hasher = sha2::Sha256::new();
        let mut buffer = Vec::new();
        File::open(path)?.read_to_end(&mut buffer)?;
        hasher.input(&buffer);
        Ok(self.checksum = hasher.result_str())
    }

    fn prepend_path(&self, path: &Path) -> PathBuf {
        let mut prepend: PathBuf = PathBuf::from("./");
        prepend.push(path);
        prepend.to_path_buf()
    }
}

#[derive(RustcDecodable,RustcEncodable,Debug,Default,PartialEq,Eq,Hash,Clone)]
pub struct PkgInfo {
    pub name: String,
    vers: String,
    desc: String,
    builddate: String,
    url: String,
    size: u64,
    arch: Vec<Arch>,
    license: String,
    conflicts: Vec<String>,
    provides: String,
    deps: Vec<String>,
    maintainers: Vec<String>,
}

impl PkgInfo {
    pub fn new(build_file: &PackageDesc) -> PkgInfo {
        let mut info: PkgInfo = Default::default();
        info.name = build_file.name.clone().unwrap_or("Unknown".to_owned());
        info.vers = build_file.vers.clone().unwrap_or("Unknown".to_owned());
        info.desc = build_file.desc.clone().unwrap_or("".to_owned());
        info.builddate = build_file.builddate.clone().unwrap_or("Unkown".to_owned());
        info.url = build_file.url.clone().unwrap_or("Uknown".to_owned());
        info.size = build_file.pkg_size().unwrap_or(0);
        info.arch = build_file.arch.clone().unwrap_or(vec![Default::default()]);;
        info.license = build_file.license.clone().unwrap_or("Unkown".to_owned());
        info.conflicts = build_file.conflicts.clone().unwrap_or(vec!["Unkown".to_owned()]);
        info.provides = build_file.provides.clone().unwrap_or("Uknown".to_owned());
        info.deps = build_file.deps.clone().unwrap_or(vec![Default::default()]);
        info.maintainers = build_file.maintainers.clone().unwrap_or(vec!["Unknown".to_owned()]);
        return info;
    }

    pub fn update_size(&mut self, build_file: &PackageDesc) {
        self.size = build_file.pkg_size().unwrap_or(0);
    }

    pub fn write(&self, path: &str) -> Result<(), PkgError> {
        print!("{}", Colour::White.bold().paint("Generating PKGINFO..."));
        File::create(path)?.write_all(&format!("{}", toml::encode(&self)).as_bytes())?;
        Ok(print!("{}\n", Colour::Green.paint("OK")))
    }
}

/// Generic trait for general descriptions
pub trait Desc<T> {
    fn from_file(file: &str, name: &str) -> Result<T, Vec<PkgError>>;
    fn from_toml_table(table: toml::Value) -> Result<T, PkgError>;
    /// Executes a vector of commands using `/usr/bin/env sh -c`. Since various environment
    /// variables are set for the build environment, these commands are run as
    /// one argument to `/usr/bin/env sh -c` for example as `/usr/bin/env sh -c
    /// command arg; command arg`
    fn exec(&self, script: &Vec<String>) -> Result<(), PkgError>;
    fn print_json(&self);
}

impl<T: Encodable + Decodable> Desc<T> for T {
    fn from_file(file: &str, name: &str) -> Result<T, Vec<PkgError>> {
        assert_toml(file)?;
        parse_toml_file(file).and_then(|toml| {
            toml.get(name)
                .ok_or(vec![PkgError::NoDesc(name.to_owned())])
                .and_then(|desc| {
                    Decodable::decode(&mut toml::Decoder::new(desc.clone()))
                        .map_err(|err| vec![PkgError::TomlDecode(err)])
                })
        })
    }

    fn from_toml_table(table: toml::Value) -> Result<T, PkgError> {
        Ok(Decodable::decode(&mut toml::Decoder::new(table))?)
    }

    fn exec(&self, script: &Vec<String>) -> Result<(), PkgError> {
        let mut script_clone = script.clone();
        for mut line in &mut script_clone {
            line.push_str(";");
        }
        let mut command = Command::new("/usr/bin/env").arg("sh")
            .arg("-c")
            .arg(script_clone.join(" "))
            .spawn()?;
        command.wait()?;
        if let Some(child_output) = command.stdout.as_mut() {
            // Child process has output
            let mut buff = String::new();
            println!("{}", child_output.read_to_string(&mut buff)?);
        };
        Ok(())
    }

    fn print_json(&self) {
        println!("{}", json::as_pretty_json(&self));
    }
}

#[test]
fn test_default_arch() {
    let system_arch = env::consts::ARCH;
    let default_arch: Arch = Default::default();
    assert_eq!(system_arch, format!("{:?}", default_arch));
}

#[test]
fn test_new_pkgbuild() {
    let pkg_build = PackageBuild::new();
    assert_eq!(pkg_build.package, None);
    assert_eq!(pkg_build.clean, None);
}

#[test]
#[should_panic]
fn test_pkgbuild_from_file_fail() {
    let pkg_build = match PackageBuild::from_file("none.toml") {
        Ok(_) => Ok(()),
        Err(_) => Err(()),
    };
    assert_eq!(pkg_build, Ok(()));
}

#[test]
fn test_pkgbuild_from_file_success() {
    let pkg_build = match PackageBuild::from_file("example/tar/PKG.toml") {
        Ok(_) => Ok(()),
        Err(_) => Err(()),
    };
    assert_eq!(pkg_build, Ok(()));
}

#[test]
#[should_panic]
fn test_pkgbuild_package() {
    let pkg_build = match PackageBuild::from_file("example/tar/PKG.toml") {
        Ok(pkg) => pkg,
        Err(_) => PackageBuild::new(),
    };
    assert_eq!(pkg_build.package(), None);
}

#[test]
#[should_panic]
fn test_pkgbuild_clean() {
    let pkg_build = match PackageBuild::from_file("example/tar/PKG.toml") {
        Ok(pkg) => pkg,
        Err(_) => PackageBuild::new(),
    };
    assert_eq!(pkg_build.clean(), None);
}

#[test]
fn test_new_empty_pkgdesc() {
    let build_file = PackageDesc::new();
    assert_eq!(build_file.name, None);
    assert_eq!(build_file.vers, None);
    assert_eq!(build_file.build, None);
}

#[test]
#[should_panic]
fn test_pkgdesc_from_file_fail() {
    let build_file = match PackageDesc::from_file("none.toml", "package") {
        Ok(_) => Ok(()),
        Err(_) => Err(()),
    };
    assert_eq!(build_file, Ok(()));
}

#[test]
fn test_pkgdesc_from_file_success() {
    let build_file = match PackageDesc::from_file("example/tar/PKG.toml", "package") {
        Ok(_) => Ok(()),
        Err(_) => Err(()),
    };
    assert_eq!(build_file, Ok(()));
}

#[test]
fn test_pkgdesc_from_toml_table_success() {
    let test_desc = PackageDesc::from_file("example/tar/PKG.toml", "package").unwrap();
    if let Ok(toml) = parse_toml_file("example/tar/PKG.toml") {
        if let Some(pkg) = toml.get("package") {
            let desc = PackageDesc::from_toml_table(pkg.clone()).unwrap();
            assert_eq!(test_desc, desc);
        };
    };
}

#[test]
fn test_pkgdesc_print_json() {
    let build_file = PackageDesc::from_file("example/tar/PKG.toml", "package").unwrap();
    build_file.print_json();
}

#[test]
fn test_new_empty_pkginfo() {
    let build_file = PackageDesc::from_file("example/tar/PKG.toml", "package").unwrap();
    let info = PkgInfo::new(&build_file);
    assert_eq!(info.name, build_file.name.unwrap_or("Unknown".to_owned()));
}

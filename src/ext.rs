// Copyright (C) 2016, Alberto Corona <ac@albertocorona.com>
// All rights reserved. This file is part of core-utils, distributed under the
// BSD 3-clause license. For full terms please see the LICENSE file.

extern crate toml;
extern crate rustc_serialize;

use error::PkgError;

use std::fs;
use std::fs::File;
use std::io::prelude::*;
use std::path::{Path, PathBuf};

/// Checks the file extension of a file and asserts that it either ends with
/// "toml" or "tml"
pub fn assert_toml(file: &str) -> Result<(), PkgError> {
    let metadata = fs::metadata(file)?;
    if metadata.is_dir() {
        return Err(PkgError::NonToml(file.to_string()));
    } else if metadata.is_file() {
        match Path::new(file).extension() {
            Some(ext) => {
                if ext != "toml" && ext != "tml" {
                    return Err(PkgError::NonToml(file.to_string()));
                }
            }
            None => {
                return Err(PkgError::NonToml(file.to_string()));
            }
        }
    }
    Ok(())
}

/// Strip 'build' from path's as using this directory is currently hard coded
/// behaviour
pub fn clean_path(path: PathBuf) -> PathBuf {
    let mut new_path = PathBuf::new();
    for component in path.components() {
        if component.as_ref() != "pkg" {
            new_path.push(component.as_ref());
        }
    }
    new_path
}

/// Parses a toml file
pub fn parse_toml_file<T: AsRef<Path>>(file: T) -> Result<toml::Table, Vec<PkgError>> {
    let mut buff = String::new();
    let mut error_vec = Vec::new();
    let mut file = match File::open(file) {
        Ok(s) => s,
        Err(e) => {
            error_vec.push(PkgError::Io(e));
            return Err(error_vec);
        }
    };
    match file.read_to_string(&mut buff) {
        Ok(s) => s,
        Err(e) => {
            error_vec.push(PkgError::Io(e));
            return Err(error_vec);
        }
    };
    let mut parser = toml::Parser::new(&buff);
    match parser.parse() {
        Some(s) => return Ok(s),
        None => {
            for err in parser.errors {
                error_vec.push(PkgError::TomlParse(err));
            }
            return Err(error_vec);
        }
    };
}

#[test]
fn test_clean_path() {
    let test_path = PathBuf::from("test/pkg/dir");
    assert_eq!(clean_path(test_path), PathBuf::from("test/dir"));
}

#[test]
#[should_panic]
fn test_parse_toml_file() {
    let table_from_file = parse_toml_file("example/PKG.toml").unwrap();
    let toml_table = toml::Table::new();
    assert_eq!(table_from_file, toml_table);
}

extern crate time;

use ansi_term::Colour;

use build::*;
use crypto::digest::Digest;
use crypto::sha2;
use curl::easy::Easy;
use error::PkgError;
use ext::clean_path;
use flate2::read::GzDecoder;
use repo::clone_repo;
use std::env;
use std::fs;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use tar::{Archive, Builder};
use walkdir::WalkDir;

// Structure for describing a package to be built
// TODO: Remove Option<T> on a lot of these, they should be mandatory
#[derive(RustcDecodable,RustcEncodable,Debug,Default,PartialEq,Clone,Eq,Hash)]
pub struct PackageDesc {
    pub name: Option<String>,
    pub vers: Option<String>,
    pub rel: Option<String>,
    pub build: Option<Vec<String>>,
    pub builddate: Option<String>,
    pub desc: Option<String>,
    pub package: Option<Vec<String>>,
    pub makedeps: Option<Vec<String>>,
    pub deps: Option<Vec<String>>,
    pub arch: Option<Vec<Arch>>,
    pub url: Option<String>,
    pub source: Option<Vec<String>>,
    pub sha256: Option<Vec<String>>,
    pub sha512: Option<Vec<String>>,
    pub signatures: Option<Vec<String>>,
    pub license: Option<String>,
    pub provides: Option<String>,
    pub conflicts: Option<Vec<String>>,
    pub maintainers: Option<Vec<String>>,
}

impl PackageDesc {
    // Creates a new blank BuldFile
    pub fn new() -> PackageDesc {
        Default::default()
    }

    pub fn set_builddate(&mut self) -> Result<(), PkgError> {
        Ok(self.builddate = Some(time::strftime("%m%d%Y%H%M%S", &time::now_utc())?))
    }

    pub fn sha_512(&self, file: &str) -> Result<String, PkgError> {
        let mut hasher = sha2::Sha512::new();
        let mut buffer = Vec::new();
        File::open(file)?.read_to_end(&mut buffer)?;
        hasher.input(&buffer);
        Ok(hasher.result_str())
    }

    pub fn sha_256(&self, file: &str) -> Result<String, PkgError> {
        let mut hasher = sha2::Sha256::new();
        let mut buffer = Vec::new();
        File::open(file)?.read_to_end(&mut buffer)?;
        hasher.input(&buffer);
        Ok(hasher.result_str())
    }

    // TODO: Fix for when the PKG.toml doesn't have a hash array
    pub fn match_hash(&self, index: usize, file: &str) -> Result<(), PkgError> {
        if self.sha512.is_some() {
            if (index >= self.sha512.as_ref().unwrap().len()) ||
               (self.sha512.as_ref().unwrap()[index].is_empty()) {
                return Err(PkgError::NoHash(file.to_owned()));
            }
            let hash = self.sha_512(&file)?;
            if (self.sha512.as_ref().unwrap()[index] != hash) &&
               (self.sha512.as_ref().unwrap()[index] != "SKIP") {
                return Err(PkgError::HashMismatch(file.to_owned(), hash));
            }
        } else if self.sha256.is_some() {
            if (index >= self.sha256.as_ref().unwrap().len()) ||
               (self.sha256.as_ref().unwrap().is_empty()) {
                return Err(PkgError::NoHash(file.to_owned()));
            }
            let hash = self.sha_256(&file)?;
            if (self.sha256.as_ref().unwrap()[index] != hash) &&
               (self.sha256.as_ref().unwrap()[index] != "SKIP") {
                return Err(PkgError::HashMismatch(file.to_owned(), hash));
            }
        }
        Ok(())
    }

    // 'touches' a tarball file using the string in 'name' as a file name
    pub fn create_tar_file(&self) -> Result<TarFile, PkgError> {
        let mut tar_filename = self.name.clone().unwrap_or("Unkown".to_owned());
        if let Some(arch) = self.arch.clone() {
            if let Some(pkg_vers) = self.vers.as_ref() {
                if let Some(pkg_rel) = self.rel.as_ref() {
                    if let Some(first) = arch.first() {
                        tar_filename.push_str(&format!("-{}-{}", pkg_vers, pkg_rel));
                        tar_filename.push_str(&format!("-{:?}", first));
                    };
                };
            } else {
                if let Some(first) = arch.first() {
                    tar_filename.push_str(&format!("-{:?}", first));
                };
            }
        };
        tar_filename.push_str(".pkg.tar");
        let mut tar_filepath = env::current_dir()?;
        tar_filepath.push(&tar_filename);
        Ok(TarFile::new(tar_filepath)?)
    }

    // Creates a package tarball
    pub fn create_pkg(&mut self) -> Result<(), PkgError> {
        let current_dir = env::current_dir()?;
        self.set_env()?;
        self.create_dirs()?;
        self.handle_source()?;
        self.build()?;
        self.package()?;
        self.set_builddate()?;
        env::set_current_dir(current_dir)?;
        PkgInfo::new(&self).write("pkg/PKGINFO")?;
        MTree::from_dir("pkg")?.write("pkg/MTREE")?;
        let tar = self.create_tar_file()?;
        let mut archive = Builder::new(tar.file);
        print!("{}", Colour::White.bold().paint("Compressing package.."));
        for entry in WalkDir::new("pkg") {
            let entry = entry?;
            let file_name = clean_path(entry.path().to_path_buf());
            let metadata = fs::metadata(entry.path())?;
            if metadata.is_file() {
                let mut file = File::open(entry.path())?;
                archive.append_file(file_name, &mut file)?;
            } else if metadata.is_dir() {
                if entry.path() != Path::new("pkg") {
                    archive.append_dir(file_name, entry.path())?;
                }
            }
        }
        // Wrap this turd up
        if let Err(e) = archive.finish() {
            return Err(PkgError::Io(e));
        } else {
            print!("{}\n", Colour::Green.paint("OK"))
        }
        Ok(println!("{} '{}' {}",
                    Colour::White.bold().paint("Package"),
                    Colour::Green.paint(tar.path.to_string_lossy()),
                    Colour::White.bold().paint("successfully built")))
    }

    pub fn set_env(&self) -> Result<(), PkgError> {
        let current_dir = env::current_dir()?;
        let mut pkg_dir = current_dir.clone();
        let mut src_dir = current_dir.clone();
        pkg_dir.push("pkg");
        src_dir.push("src");
        env::set_var("pkg_dir", pkg_dir);
        env::set_var("src_dir", src_dir);
        if let Some(pkg_vers) = self.vers.as_ref() {
            env::set_var("pkg_vers", pkg_vers);
        }
        if let Some(pkg_rel) = self.rel.as_ref() {
            env::set_var("pkg_rel", pkg_rel);
        };
        Ok(())
    }

    pub fn create_dirs(&self) -> Result<(), PkgError> {
        let current_dir = env::current_dir()?;
        let mut pkg_dir = current_dir.clone();
        let mut src_dir = current_dir.clone();
        pkg_dir.push("pkg");
        src_dir.push("src");
        // Don't create src_dir since it should be created by 'handle_source'
        if !pkg_dir.exists() {
            fs::create_dir(&pkg_dir)?;
        }
        Ok(())
    }

    pub fn handle_source(&self) -> Result<(), PkgError> {
        if let Some(source) = self.source.as_ref() {
            for (index, item) in source.iter().enumerate() {
                if let Some(pos) = item.find('+') {
                    let (cvs, url) = item.split_at(pos);
                    let url = url.replace("+", "");
                    match cvs.as_ref() {
                        "git" => {
                            println!("{} {}", Colour::White.paint("Cloning"), &url);
                            clone_repo(&url)?;
                        }
                        _ => (),
                    };
                } else {
                    if self.is_web_get(item) {
                        self.web_get(item)?;
                        if let Some(file_name) = item.rsplit('/').nth(0) {
                            self.match_hash(index, file_name)?;
                            // TODO fix this for signature files
                            self.extract_tar(file_name)?;
                        }
                    } else {
                        self.match_hash(index, item)?;
                    }
                }
            }
        };
        Ok(())
    }

    pub fn is_web_get(&self, url: &str) -> bool {
        if let Some(_) = url.find("://") {
            return true;
        } else {
            return false;
        }
    }

    pub fn web_get(&self, url: &str) -> Result<(), PkgError> {
        if let Some(file_name) = url.rsplit('/').nth(0) {
            println!("Downloading: {}", Colour::White.bold().paint(url));
            let mut easy = Easy::new();
            let mut buffer = Vec::new();
            easy.follow_location(true)?;
            easy.url(url)?;
            {
                let mut transfer = easy.transfer();
                transfer.write_function(|data| {
                        buffer.extend_from_slice(data);
                        Ok(data.len())
                    })?;
                transfer.perform()?
            }
            let mut file = File::create(file_name)?;
            file.write(&mut buffer)?;
        }
        Ok(())
    }

    pub fn extract_tar(&self, path: &str) -> Result<(), PkgError> {
        if let Ok(decomp) = GzDecoder::new(File::open(path)?) {
            let mut archive = Archive::new(decomp);
            archive.unpack("src")?;
        } else {
            let mut archive = Archive::new(File::open(path)?);
            archive.unpack("src")?;
        };
        Ok(())
    }

    pub fn build(&self) -> Result<(), PkgError> {
        env::set_current_dir("src")?;
        println!("{}", Colour::White.bold().paint("Beginning build"));
        if let Some(script) = self.build.as_ref() {
            self.exec(script)?;
        };
        Ok(println!("{}", Colour::White.bold().paint("Build succeeded")))
    }

    pub fn package(&self) -> Result<(), PkgError> {
        println!("{}", Colour::White.bold().paint("Beginning package"));
        if let Some(script) = self.package.as_ref() {
            self.exec(script)?;
        };
        Ok(println!("{}", Colour::White.bold().paint("Package succeeded")))
    }

    pub fn pkg_size(&self) -> Result<u64, PkgError> {
        let mut size: u64 = 0;
        for entry in WalkDir::new("pkg") {
            let entry = entry?;
            if entry.path() != Path::new("pkg") {
                match fs::metadata(entry.path()) {
                    Ok(s) => size += s.len(),
                    Err(e) => return Err(PkgError::Io(e)),
                };
            }
        }
        Ok(size)
    }
}
